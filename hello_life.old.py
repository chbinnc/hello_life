import sensor, lcd, image
import time
import ulab as np

clock = time.clock()
lcd.init(freq=15000000)
#lcd.init()
lcd.set_backlight(0)
sensor.reset()
sensor.set_pixformat(sensor.GRAYSCALE)
sensor.set_framesize(sensor.QVGA)
sensor.run(1)
img = sensor.snapshot()
#img = img.to_grayscale()
RATIO = 3
WIDTH = img.width()//RATIO
HEIGHT = img.height()//RATIO
sensor.run(0)
for x in range(WIDTH):
    for y in range(HEIGHT):
	if img.get_pixel(x,y) % 2 == 0:
	    img.set_pixel(x,y,0)
	else:
	    img.set_pixel(x,y,200)
lcd.display(img)
img = img.copy([0,0,WIDTH,HEIGHT])
lcd.display(img)

def imgToMatrix(img, matrix):
    for x in range(WIDTH):
	for y in range(HEIGHT):
	    matrix[(x,y)] = img.get_pixel(x,y)
    return matrix

def listSurrounding():
    list_surrounding = []
    for i in [-1,0,1]:
	for j in [-1,0,1]:
	    if [i,j] != [0,0]:
		list_surrounding.append([i,j])
    return list_surrounding

def sumSurrounding3(matrix):
    matrix_extended = np.zeros((WIDTH+2, HEIGHT+2), dtype=np.uint16)
    #result = np.zeros((WIDTH, HEIGHT), dtype=np.uint16)
    #print("Result0: ", result)
    matrix_extended[1:-1, 1:-1] = matrix[:, :]
    matrix_extended[(0,0)] = matrix[(-1,-1)]
    matrix_extended[(0,-1)] = matrix[(-1,0)]
    matrix_extended[(-1,0)] = matrix[(0,-1)]
    matrix_extended[(-1,-1)] = matrix[(0,0)]
    #print("Result1: ", result)

    matrix_extended[0:1, 1:-1] = matrix[-1:, :]
    matrix_extended[-1, 1:-1] = matrix[0:1:, :]
    matrix_extended[1:-1, 0:1] = matrix[:, -1:]
    matrix_extended[1:-1, -1:] = matrix[:, 0:1]
    #print("Compare: ", matrix_extended[0:1, 1:-1], matrix[-1:, :])
    #print("Result2: ", matrix_extended, matrix_extended[0:1, 1:-1])

    #print("Extended Matrix: ", result.shape())
    #print("Extended Matrix1: ", result[0:WIDTH, 0:HEIGHT], result[0:WIDTH, 0:HEIGHT].shape())
    #print("Extended Matrix2: ", result[0:WIDTH, 1:HEIGHT+1], result[0:WIDTH, 1:HEIGHT+1].shape())
    #print("Extended Matrix3: ", result[0:WIDTH, 2:].shape())
    #print("Extended Matrix4: ", result[2:, 2:].shape())
    #print("Test merge: ", result[0:WIDTH, 0:HEIGHT] + result[0:WIDTH, 1:HEIGHT+1])


    return matrix_extended[0:WIDTH, 0:HEIGHT] + matrix_extended[0:WIDTH, 1:HEIGHT+1] + \
	    matrix_extended[0:WIDTH, 2:] + matrix_extended[1:WIDTH+1, 0:HEIGHT] + \
	    matrix_extended[1:WIDTH+1, 2:] + matrix_extended[2:, 0:HEIGHT] + \
	    matrix_extended[2:, 1:HEIGHT+1] + matrix_extended[2:, 2:]
    #print(result)
    #return result

def sumSurrounding(x,y,img):
    result = 0
    for pair in list_surrounding:
	position = [sum(i) for i in zip(pair,[x,y])]
	if position[0] == -1:
	    position[0] = WIDTH - 1
	if position[1] == -1:
	    position[1] = HEIGHT - 1
	if position[0] == WIDTH:
	    position[0] = 0
	if position[1] == HEIGHT:
	    position[1] = 0
	#if img.get_pixel(position) != None:
	result += img.get_pixel(position)
    return result

# img2 = img.copy(copy_to_fb=True) # just shallow copy
#img2 = img.copy([0,0,WIDTH,HEIGHT])

matrix_data = np.zeros((WIDTH, HEIGHT), dtype=np.uint16)
matrix_data = imgToMatrix(img,matrix_data)

while True:
    clock.tick()

    ## Surrounding
    # for x in range(WIDTH):
	# for y in range(HEIGHT):
	    # list_surrounding = listSurrounding()
	    # sum_surrounding = sumSurrounding(x,y,img)
	    # #sum_surrounding = 0
	    # if img.get_pixel(x,y) == 0:
		# if sum_surrounding == 600:
		    # img2.set_pixel(x,y,200)
	    # else:
		# if sum_surrounding < 400 or sum_surrounding > 600:
		    # img2.set_pixel(x,y,0)
    # img = img2.copy()
    # fps = clock.fps()
    # img2.draw_string(2,2, ("%2.4ffps" %(fps)), color=200, scale=2, mono_space=False)
    # lcd.display(img2)
    # img2 = img.copy()

    ## Surrounding3
    #print("Matrix data 0: ", matrix_data)
    #matrix_data = imgToMatrix(img, matrix_data)
    #print("Matrix data: ", matrix_data[:10,:10])
    #break
    matrix_sum_surrounding = sumSurrounding3(matrix_data)
    #print(matrix_sum_surrounding[:10,:10])
    #print(matrix_data[:10,:10])
    #break
    img.clear()
    for x in range(WIDTH):
	for y in range(HEIGHT):
	    sum_surrounding = matrix_sum_surrounding[x][y]
	    if matrix_data[x][y] == 0:
		#print('point0')
		if sum_surrounding == 600:
		    matrix_data[(x,y)] = 200
		    img.set_pixel(x,y,200)
		    #print('point1')
		    #break
	    else:
		if sum_surrounding < 400 or sum_surrounding > 600:
		    matrix_data[(x,y)] = 0
		    img.set_pixel(x,y,0)
		    #print('point2')
		    #break
    fps = clock.fps()
    img.draw_string(2,2, ("%2.4ffps" %(fps)), color=200, scale=2, mono_space=False)
    lcd.display(img)
