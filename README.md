Just a little project for practicing.

`hello_life.py` uses camera to generate initial universe.

`hello_life_jupyter_lab.py` is for testing main function on Jupyter Lab.

`hello_high_life.py` is an example discovered from https://f-droid.org/packages/org.jtb.droidlife/

`hello_life.old.py` is a pre-release version with more commented code for debug.

`main.py` is for executing `hello_life.py` or other file, so, simply copy `main.py` and `hello_life.py` to the sdcard or flash memory of your board, that's all for running this project.

Board info: https://www.seeedstudio.com/Sipeed-Maixduino-Kit-for-RISC-V-AI-IoT-p-4047.html
