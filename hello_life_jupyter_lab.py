import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from IPython.display import HTML
import time

WIDTH = 50
HEIGHT = 50
img = np.random.randint(2,size=(WIDTH, HEIGHT)) * 200

list_neighbors = []
for i in [-1,0,1]:
    for j in [-1,0,1]:
        if (i, j) != (0, 0):
            list_neighbors.append([i,j])

def sumNeighbors(x,y,img):
    result = 0
    for pair in list_neighbors:
        position = [sum(i) for i in zip(pair,[x,y])]
        if position[0] == -1:
            position[0] = WIDTH - 1
        if position[1] == -1:
            position[1] = HEIGHT - 1
        if position[0] == WIDTH:
            position[0] = 0
        if position[1] == HEIGHT:
            position[1] = 0
        #if img.get_pixel(position) != None:
        #result += img.get_pixel(position)
        result += img[position[0]][position[1]]
    return result

def sumNeighbors2(img):
    neighbor_up = np.vstack([ img[-1:, :], img[:-1, :] ])
    neighbor_down = np.vstack([ img[1:, :], img[0:1, :] ])
    neighbor_left = np.hstack([ img[:, -1:], img[:, :-1] ])
    neighbor_right = np.hstack([ img[:, 1: ], img[:, 0:1] ])
    neighbor_up_left = np.hstack([ neighbor_up[:, -1:], neighbor_up[:, :-1] ])
    neighbor_up_right = np.hstack([ neighbor_up[:, 1: ], neighbor_up[:, 0:1] ])
    neighbor_down_left = np.hstack([ neighbor_down[:, -1:], neighbor_down[:, :-1] ])
    neighbor_down_right = np.hstack([ neighbor_down[:, 1: ], neighbor_down[:, 0:1] ])
    return neighbor_up + neighbor_down + neighbor_left + neighbor_right + \
            neighbor_up_left + neighbor_up_right + neighbor_down_left + neighbor_down_right

def sumNeighbors3(img):
    result = np.zeros((WIDTH+2, HEIGHT+2))
    result[1:-1, 1:-1] = img[:, :]
    result[0][0] = img[-1][-1]
    result[0][-1] = img[-1][0]
    result[-1][0] = img[0][-1]
    result[-1][-1] = img[0][0]
    result[0:1, 1:-1] = img[-1:, :]
    result[-1, 1:-1] = img[0:1:, :]
    result[1:-1, 0:1] = img[:, -1:]
    result[1:-1, -1:] = img[:, 0:1]

    return result[0:WIDTH, 0:HEIGHT] + result[0:WIDTH, 1:HEIGHT+1] + result[0:WIDTH, 2:] + \
            result[1:WIDTH+1, 0:HEIGHT] + result[1:WIDTH+1, 2:] + \
            result[2:, 0:HEIGHT] + result[2:, 1:HEIGHT+1] + result[2:, 2:]

fig = plt.figure(figsize=(10, 10))
ims = []
for i in range(50):
    img2 = img.copy()
    sum_img_neighbor = sumNeighbors3(img)
    for x in range(WIDTH):
        for y in range(HEIGHT):
            #sum_neighbors = sumNeighbors(x,y,img) ##
            sum_neighbors = sum_img_neighbor[x,y]
            if img[x][y] == 0:
                if sum_neighbors == 600:
                    img2[x][y] = 200
            else:
                if sum_neighbors < 400 or sum_neighbors > 600:
                    img2[x][y] = 0
    img = img2
    im = plt.imshow(img2, animated=True)
    ims.append([im])

ani = animation.ArtistAnimation(fig, ims, interval=100, blit=True, repeat_delay=1000)
plt.close()

# Show the animation
HTML(ani.to_html5_video())
