import sensor, lcd, image
import time #as time
import ulab as np

from Maix import GPIO
from board import board_info
from fpioa_manager import fm

RATIO = 2 # the ratio of the screen to the size of image.

lcd.init(freq=15000000)
#lcd.init()
#lcd.set_backlight(0)
sensor.reset()
sensor.set_pixformat(sensor.GRAYSCALE)
sensor.set_framesize(sensor.QVGA)
sensor.run(1)
img = sensor.snapshot()
#img = img.to_grayscale()
sensor.run(0)

# Register boot button
fm.register(board_info.BOOT_KEY, fm.fpioa.GPIOHS1)
key = GPIO(GPIO.GPIOHS1, GPIO.IN)

start_time = time.ticks()//1000
#start_time = time.time()
clock = time.clock()

WIDTH = int(img.width()/RATIO)
HEIGHT = int(img.height()//RATIO)

matrix_data = np.zeros((WIDTH, HEIGHT), dtype=np.uint8)

for x in range(WIDTH):
    for y in range(HEIGHT):
        if img.get_pixel(x,y) % 2 == 0:
            img.set_pixel(x,y,0)
        else:
            img.set_pixel(x,y,200)
            matrix_data[(x,y)] = 1
lcd.display(img)
# img2 = img.copy(copy_to_fb=True) # just shallow copy
img = img.copy([0,0,WIDTH,HEIGHT+30])
lcd.display(img)

def sumNeighbors3(matrix):
    matrix_extended = np.zeros((WIDTH+2, HEIGHT+2), dtype=np.uint8)
    matrix_extended[1:-1, 1:-1] = matrix[:, :]
    matrix_extended[(0,0)] = matrix[(-1,-1)]
    matrix_extended[(0,-1)] = matrix[(-1,0)]
    matrix_extended[(-1,0)] = matrix[(0,-1)]
    matrix_extended[(-1,-1)] = matrix[(0,0)]

    matrix_extended[0:1, 1:-1] = matrix[-1:, :]
    matrix_extended[-1, 1:-1] = matrix[0:1:, :]
    matrix_extended[1:-1, 0:1] = matrix[:, -1:]
    matrix_extended[1:-1, -1:] = matrix[:, 0:1]

    return matrix_extended[0:WIDTH, 0:HEIGHT] + matrix_extended[0:WIDTH, 1:HEIGHT+1] + \
            matrix_extended[0:WIDTH, 2:] + matrix_extended[1:WIDTH+1, 0:HEIGHT] + \
            matrix_extended[1:WIDTH+1, 2:] + matrix_extended[2:, 0:HEIGHT] + \
            matrix_extended[2:, 1:HEIGHT+1] + matrix_extended[2:, 2:]

while True:
    clock.tick()

    ## Surrounding3
    matrix_sum_neighbors = sumNeighbors3(matrix_data)
    img.clear()
    for x in range(WIDTH):
        for y in range(HEIGHT):
            sum_neighbors = matrix_sum_neighbors[x][y]
            if matrix_data[x][y] == 0:
                if sum_neighbors == 3 or sum_neighbors == 6:
                    matrix_data[(x,y)] = 1
                    img.set_pixel(x,y,200)
            else:
                if sum_neighbors < 2 or sum_neighbors > 3:
                    matrix_data[(x,y)] = 0
                    img.set_pixel(x,y,0)
    running_time = (time.ticks()//1000 - start_time)
    #tmp_time = running_time // 26000000
    #if tmp_time > 0:
    #    running_time = tmp_time
    fps = clock.fps()
    img.draw_string(40,HEIGHT + 5, ("%2.2ffps %2.0fs" %(fps, running_time)), color=200, scale=1.4) #, mono_space=False)
    lcd.display(img)

    if key.value() == 0:
        break

fm.unregister(board_info.BOOT_KEY,fm.fpioa.GPIOHS1)
